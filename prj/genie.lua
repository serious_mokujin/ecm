-- A solution contains projects, and defines the available configurations
solution "ECM"
   location(_ACTION)
   configurations { "Debug", "Release" }
 
   -- A project defines one build target
   project "EntityComponentModel"
      kind "ConsoleApp"
      language "C++"
      files { "../src/**.hpp", "../src/**.cpp" }
 
      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols" }
 
      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize" }    