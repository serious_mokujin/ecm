#ifndef COMPONENT_MODEL
#define COMPONENT_MODEL



#include "ETypedefs.hpp"

#include "Archive.hpp"
#include "Entity.hpp"
#include "ComponentType.hpp"
#include "Component.hpp"
#include "ComponentRegister.hpp"
#include "TagManager.hpp"
#include "Aspect.hpp"
#include "ESystem.hpp"
#include "SystemRegister.hpp"
#include "Manager.hpp"
#include "EWorld.hpp"

#endif