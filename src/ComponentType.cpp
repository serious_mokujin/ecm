#include "ComponentType.hpp"
#include "ETypedefs.hpp"

ComponentType::ComponentType(const std::string& name) :m_name(name)
{
	static int nextID = 0;
	static ComponentMask nextBit(0);
	nextBit <<= 1;
	if (nextBit.none())
		nextBit.set(0, 1);
	m_bit = ComponentMask(nextBit);
	m_id = nextID++;
	s_componentTypesCount++;
}
