#ifndef PRINT_SYSTEM_H
#define PRINT_SYSTEM_H

#include "NameComponent.hpp"
#include "PositionComponent.hpp"
#include "VelocityComponent.hpp"

#include "SystemRegister.hpp"
#include "ESystem.hpp"

class PrintSystem : public ESystem
{
protected:
	virtual void Initialize() override;
	virtual void Utilize() override;
	virtual RunProcessingFlags BeginProcessing() override;
	virtual void ProcessEntity(int entityID) override;
};

SYSTEM_REGISTER_DECL(PrintSystem)

#endif