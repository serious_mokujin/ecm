#include "PositionComponent.hpp"
#include "Archive.hpp"

COMPONENT_REGISTER_IMPL(PositionComponent)

void PositionComponent::Init()
{
	posX = 2;
	posY = 5;
}

void PositionComponent::Serialize(Archive& stream)
{
	if(stream.GetDirection() == Archive::Direction::Store)
		stream<<posX<<posY;
	else
		stream>>posX>>posY;
}


