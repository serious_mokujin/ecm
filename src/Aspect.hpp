#pragma once
#include "ETypedefs.hpp"

class Entity;

class Aspect
{
	ComponentMask anyTypeMask;
	ComponentMask noneTypeMask;
	ComponentMask allTypeMask;

	TagMask anyTagMask;
	TagMask noneTagMask;
	TagMask allTagMask;
public:
	Aspect();
	template<class T> Aspect& AllOfTypes();

	template<class T> Aspect& AnyOfTypes();

	template<class T> Aspect& NoneOfTypes();

	Aspect& AllOfTypes(const std::string& typeName);
	Aspect& AnyOfTypes(const std::string& typeName);
	Aspect& NoneOfTypes(const std::string& typeName);

	Aspect& AllOfTags(const std::string& tagName);
	Aspect& AnyOfTags(const std::string& tagName);
	Aspect& NoneOfTags(const std::string& tagName);

	bool Intersects(Entity& entity);
};

template<class T>
inline Aspect& Aspect::AllOfTypes()
{
	ComponentType* t = const_cast<ComponentType*>(Component::GetType<T>());
	allTypeMask |= t->GetBit();
	return *this;
}

template<class T>
inline Aspect& Aspect::AnyOfTypes()
{
	ComponentType* t = const_cast<ComponentType*>(Component::GetType<T>());
	anyTypeMask |= t->GetBit();
	return *this;
}

template<class T>
inline Aspect& Aspect::NoneOfTypes()
{
	ComponentType* t = const_cast<ComponentType*>(Component::GetType<T>());
	noneTypeMask |= t->GetBit();
	return *this;
}