#ifndef ARCHIVE_FILE_HEADER
#define ARCHIVE_FILE_HEADER

#include <iostream>
#include "ETypedefs.hpp"
#include "Archive.hpp"

class ArchiveFile: public Archive
{
private:
    std::fstream* m_stream;
public:
	ArchiveFile(const std::string& file, bool read);
    virtual ~ArchiveFile();

    virtual void write(const void *buffer, size_t length) override;  
    virtual void read (void* buffer, size_t length) override;
	virtual void end() override;
};

#endif