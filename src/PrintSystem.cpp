#include "PrintSystem.hpp"
#include "EWorld.hpp"
#include <cstdio>

SYSTEM_REGISTER_IMPL(PrintSystem)

RunProcessingFlags PrintSystem::BeginProcessing()
{
	return RunProcessingFlags::run_per_entity;
}

void PrintSystem::ProcessEntity(int entityID)
{
	NameComponent* name = EWorld::Please()->GetComponent<NameComponent>(entityID);
	PositionComponent* pos = static_cast<PositionComponent*>(EWorld::Please()->GetComponent(entityID, "PositionComponent"));
	VelocityComponent* vel = EWorld::Please()->GetComponent<VelocityComponent>(entityID);
    printf("[%s]: ", name->name.c_str());
	if(pos != nullptr)
		printf("Position: (x: %d, y: %d) \n", pos->posX, pos->posY);
	if(vel != nullptr)
		printf("Velocity: (x: %f, y: %f) \n", vel->speedX, vel->speedY);
}

void PrintSystem::Initialize()
{
	SetAspect(Aspect().AllOfTypes("NameComponent").AnyOfTypes<PositionComponent>().AnyOfTypes<VelocityComponent>().AllOfTags("plr"));
}

void PrintSystem::Utilize()
{

}
