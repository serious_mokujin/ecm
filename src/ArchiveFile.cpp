#include "ArchiveFile.hpp"
#include <fstream>

ArchiveFile::ArchiveFile(const std::string& file, bool read) : Archive(read ? Archive::Direction::Load : Archive::Direction::Store)
{
	m_stream = new std::fstream(file.c_str(), (read ? std::ios::in : std::ios::out) | std::ios::binary);
}
ArchiveFile::~ArchiveFile() 
{
	m_stream->close();
	delete m_stream;
}

void ArchiveFile::write(const void* buffer, size_t length)
{
    m_stream->write((const char*)buffer,length);
    if(! *m_stream)
        throw "ArchiveFile::write Error";
}
void ArchiveFile::end()
{
	m_stream->close();
}
void ArchiveFile::read(void* buffer, size_t length)
{
    m_stream->read((char*)buffer, length);
    if(! *m_stream)
        throw "ArchiveFile::read Error";    
}