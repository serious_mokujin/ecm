#include "ETypedefs.hpp"
#include "Entity.hpp"
#include "Archive.hpp"
#include "ComponentType.hpp"
#include "Component.hpp"
#include "TagManager.hpp"
#include "Aspect.hpp"
#include <algorithm>
#include "ESystem.hpp"

ESystem::ESystem() : m_enabled(false)
{
	static SystemMask nextBit(0);
	nextBit <<= 1;
	if(nextBit.none())
		nextBit.set(0, 1);
	m_systemBit = nextBit;
}

ESystem::~ESystem(void)
{
}

void ESystem::Clear()
{
	m_entityIDs.clear();
}

void ESystem::SetAspect(const Aspect &aspect)
{
	m_aspect = aspect;
}

const Aspect& ESystem::GetAspect() const
{
	return m_aspect;
}

SystemMask& ESystem::GetBit()
{
	return m_systemBit;
}

void ESystem::CheckEntity(Entity& entity)
{
	bool contains = (entity.systemBits & m_systemBit).any();
	bool intersects = m_aspect.Intersects(entity);
	if(intersects && !contains && !entity.recycled)
	{
		m_entityIDs.push_back(entity.id);
		entity.systemBits |= m_systemBit;
	}
	else if((!intersects && contains) || (contains && entity.recycled))
	{
		m_entityIDs.erase(std::find(m_entityIDs.begin(), m_entityIDs.end(), entity.id));
		SystemMask temp(m_systemBit);
		entity.systemBits &= temp.flip();
	}
}

void ESystem::RunProcessing()
{
	if(!m_enabled) return;
	RunProcessingFlags flags = BeginProcessing();
	if((flags & RunProcessingFlags::do_not_run) == 0)
	{
		if(flags & RunProcessingFlags::run_per_entity)
		{
			for(std::vector<int>::iterator i = m_entityIDs.begin(); i != m_entityIDs.end(); ++i)
				ProcessEntity(*i);
		}
		if(flags & RunProcessingFlags::run_whole_at_once)
		{
			ProcessHeap(m_entityIDs.begin(), m_entityIDs.end());
		}
		if(flags & RunProcessingFlags::run_with_each_other)
		{
			for(std::vector<int>::iterator i = m_entityIDs.begin(); i != m_entityIDs.end(); ++i)
				for(std::vector<int>::iterator j = i + 1; j != m_entityIDs.end(); ++j)
					ProcessEachOther(*i, *j);
		}
	}
	EndProcessing();
}
