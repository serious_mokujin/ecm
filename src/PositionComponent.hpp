#ifndef POSITION_COMPONENT_H
#define POSITION_COMPONENT_H

#include "ComponentRegister.hpp"
#include "Component.hpp"

class Archive;

class PositionComponent : public Component
{
public:
	int posX, posY;
	virtual void Init() override;
	virtual void Serialize(Archive& stream) override;
};

COMPONENT_REGISTER_DECL(PositionComponent)

#endif