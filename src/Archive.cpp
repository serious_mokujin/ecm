#include "Archive.hpp"
#include "ETypedefs.hpp"

Archive::Archive(Archive::Direction direction)
	: m_direction(direction) 
{
}

Archive::~Archive()
{
}

Archive& Archive::operator<<(int val)
{
     write(&val, sizeof(int));
     return *this;
}

Archive& Archive::operator>>(int& val)
{
     read(&val, sizeof(int));
     return *this;
}

Archive& Archive::operator<<(float val)
{
     write(&val, sizeof(float));
     return *this;
}

Archive& Archive::operator>>(float& val)
{
     read(&val, sizeof(float));
     return *this;
}

Archive& Archive::operator<<(const std::string& str)
{ 
    int length = str.length();
    *this << length;
    write(str.c_str(), sizeof(char) * length);
    return *this;
}

Archive& Archive::operator>>(std::string& str)
{
    int length = -1;
    *this >> length;
    std::vector<char> mem(length + 1);
    char* pChars = &mem[0];
    read(pChars, sizeof(char) * length);
    mem[length] = NULL;
    str = pChars;
    return *this;
}

Archive& Archive::operator<<(const ComponentMask& val)
{
	int len = (int)val.size();
	write(&len, sizeof(int)); 
	for(int i = 0; i < len; i++)
	{
		int buffer = val[i];
		write(&buffer, 1); 
	}
	return *this;
}

Archive& Archive::operator>>(ComponentMask& val)
{
	int len = 0;
	read(&len, sizeof(int));
	for(int i = 0; i<len; i++)
	{
		int buffer = 0;
		read(&buffer, 1);
		if(buffer != 0)
			val.set(i);
		else
			val.reset(i);
	}
	return *this;
}

Archive& Archive::operator<<(const TagMask& val)
{
	int len = (int)val.size();
	write(&len, sizeof(int)); 
	for(int i = 0; i<len; i++)
	{
		int buffer = val[i];
		write(&buffer, 1); 
	}
	return *this;
}

Archive& Archive::operator>>(TagMask& val)
{
	int len = 0;
	read(&len, sizeof(int));
	for(int i = 0; i<len; i++)
	{
		int buffer = 0;
		read(&buffer, 1);
		if(buffer != 0)
			val.set(i);
		else
			val.reset(i);
	}
	return *this;
}