#ifndef ETYPEDEFS_GUARD_H
#define ETYPEDEFS_GUARD_H

#include <bitset>
#include <vector>
#include <string>

#define MAX_COMPONENTS 64
#define MAX_TAGS 16
#define MAX_SYSTEMS 16

typedef std::bitset<MAX_COMPONENTS> ComponentMask;
typedef std::bitset<MAX_SYSTEMS> SystemMask;
typedef std::bitset<MAX_TAGS> TagMask;

#endif