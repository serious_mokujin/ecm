#ifndef COMPONENT_REGISTER_HEADER
#define COMPONENT_REGISTER_HEADER

#include "ComponentType.hpp"

#define COMPONENT_REGISTER_DECL(TYPE)									\
template<>																	\
class ComponentRegistration<TYPE>											\
{																			\
public:																		\
    static const RegistryType<TYPE>& reg;									\
	static const ComponentType& type;										\
};																				

#define COMPONENT_REGISTER_IMPL(TYPE)															\
const ComponentType& ComponentRegistration<TYPE>::type = RegistryType<TYPE>::TypeInstance(#TYPE);	\
const RegistryType<TYPE>& ComponentRegistration<TYPE>::reg = RegistryType<TYPE>::Instance();	


#endif