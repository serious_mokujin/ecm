#pragma once
#include <unordered_map>
#include "ETypedefs.hpp"
class EWorld;
class Aspect;

class Entity
{
public:
	Entity(int _id) : id(_id){}
	int id;
	ComponentMask currentComponentBits;
	ComponentMask nextComponentBits;
	TagMask tagBits;
	SystemMask systemBits;
	std::unordered_map<int, int> componentLocations;
	bool recycled;

	void SetComponentLocation(int typeId, int location);
	int GetComponentLocation(int typeId);
};
inline void Entity::SetComponentLocation(int typeId, int location)
{
	componentLocations[typeId] = location;
}
inline int Entity::GetComponentLocation(int typeId)
{
	return componentLocations[typeId];
}