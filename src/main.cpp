#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include <cstdio>

#include "PositionComponent.hpp"
#include "VelocityComponent.hpp"
#include "NameComponent.hpp"
#include "PrintSystem.hpp"
#include "EWorld.hpp"
#include <windows.h>
#include "Aspect.hpp"
#include "ArchiveFile.hpp"

//#define READ_COMPONENT
#define WRITE_COMPONENT
int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(159);
	EWorld::Please()->Initialize();
	EWorld::Please()->EnableSystem<PrintSystem>();
	int entityID = EWorld::Please()->AddEntity();
	EWorld::Please()->AddComponent<PositionComponent>(entityID);
	EWorld::Please()->AddTagEntity(entityID, "plr");
	EWorld::Please()->AddTagEntity(entityID, "printable");
	EWorld::Please()->RemoveTagEntity(entityID, "printable");
	EWorld::Please()->AddTagEntity(entityID, "printable");
	EWorld::Please()->AddTagEntity(entityID, "lol");
	EWorld::Please()->AddTagEntity(entityID, "pri");
	EWorld::Please()->RemoveTagEntity(entityID, "lol");

	EWorld::Please()->AddComponent(entityID, "VelocityComponent");
	EWorld::Please()->RemoveComponent<VelocityComponent>(entityID);
	EWorld::Please()->AddComponent(entityID, "VelocityComponent");
	EWorld::Please()->AddComponent<NameComponent>(entityID);
	EWorld::Please()->Update();
	EWorld::Please()->GetComponent<NameComponent>(entityID)->SetName("Player");
	EWorld::Please()->RemoveComponent<VelocityComponent>(entityID);
	EWorld::Please()->Update();
	EWorld::Please()->AddComponent(entityID, "VelocityComponent");
	EWorld::Please()->Update();
	EWorld::Please()->Update();

	EWorld::Please()->GetComponent<VelocityComponent>(entityID)->speedX = 7.1f;
	EWorld::Please()->GetComponent<VelocityComponent>(entityID)->speedY = 0.75f;

	EWorld::Please()->GetComponent<PositionComponent>(entityID)->posX = 9;
	EWorld::Please()->GetComponent<PositionComponent>(entityID)->posY = 784;
	
#ifdef READ_COMPONENT
	ArchiveFile stream("dump.dat", true);
	EWorld::Please()->Load(static_cast<Archive*>(&stream));
	stream.end();
#endif
#ifdef WRITE_COMPONENT
	ArchiveFile stream("dump.dat", false);
	EWorld::Please()->Store(static_cast<Archive*>(&stream));
	stream.end();
#endif

	printf("Entity component model\n");
	
	EWorld::Please()->ProcessSystem<PrintSystem>();

	EWorld::Thanks();
	_CrtDumpMemoryLeaks();
	getchar();
	return 0;
}