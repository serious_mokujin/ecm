#include "ETypedefs.hpp"
#include "Entity.hpp"
#include "Archive.hpp"
#include "ComponentType.hpp"
#include "Component.hpp"

#include "TagManager.hpp"
#include "Aspect.hpp"

#include "ESystem.hpp"
#include "SystemRegister.hpp"
	
ESystem* getSystem(const std::string& name)	
{
	NameToSystem& m = getNameToSystemMap();
	auto it = m.find(name);
	if(it != m.end())
		return it->second;
	return nullptr;
}