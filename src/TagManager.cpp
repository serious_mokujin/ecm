#include "ETypedefs.hpp"
#include "TagManager.hpp"

std::unordered_map<std::string, TagMask> TagManager::m_tags;

TagMask TagManager::GetTagMask(const std::string& tagName)
{
	static TagMask nextTag(0);

	auto it = m_tags.find(tagName);
	if (it != m_tags.end())
		return it->second;
	else
	{
		nextTag <<= 1;
		if (nextTag.to_ulong() == 0)
			nextTag.set(0, 1);
		m_tags.insert(std::unordered_map<std::string, TagMask>::value_type(tagName, nextTag));
		return nextTag;
	}
}