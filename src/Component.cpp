#include "ETypedefs.hpp"
#include "Entity.hpp"
#include "Archive.hpp"
#include "ComponentType.hpp"
#include "Component.hpp"

Component::Component()
{
}

Component::~Component()
{}

ComponentType* Component::GetType(const std::string& name)
{
	NameToIdMap& nameToIdMap = getNameToIdMap();
	IdToTypeMap& typeMap = getComponentTypeMap();
    auto it = nameToIdMap.find(name);
    if (it != nameToIdMap.end()) 
	{
		auto it2 = typeMap.find(it->second);
		if(it2 != typeMap.end())
			return &it2->second;
    }
	return nullptr;
}
ComponentType* Component::GetType(int typeId)
{
	IdToTypeMap& typeMap = getComponentTypeMap();
	auto it = typeMap.find(typeId);
	if(it != typeMap.end())
		return &it->second;
	return nullptr;
}