#ifndef SYSTEM_REGISTER_HEADER
#define SYSTEM_REGISTER_HEADER

//#include "ComponentModel.hpp"
#include <unordered_map>
#include <string>
//#include "ESystem.hpp"

template<class T>
class SystemRegistration;
class ESystem;

typedef std::unordered_map<std::string, ESystem*> NameToSystem;
typedef std::vector<ESystem*> SystemBucket;
typedef ESystem* (*GetSystemByType)();
typedef ESystem* (*GetSystemByName)(const std::string&);

inline NameToSystem& getNameToSystemMap()
{
	static NameToSystem map;
	return map;
}
inline SystemBucket& getSystemBucket()
{
	static SystemBucket reg;
	return reg;
}

template<class T>							
ESystem* getSystem()	
{
	return SystemRegistration<T>::reg.system;
}					
ESystem* getSystem(const std::string& name);

template<class T>
class RegistrySystem
{
public:
	static RegistrySystem<T>& Instance(const std::string& name)
	{
		static RegistrySystem<T> inst(name);
        return inst;
	}
	T* system;
private:

	RegistrySystem(const std::string& name)
	{
		system = new T();
		NameToSystem& m = getNameToSystemMap();
		m.insert(NameToSystem::value_type(name, system));
		SystemBucket& reg = getSystemBucket();
		reg.push_back(system);
	}
	~RegistrySystem()
	{
		delete system;
	}
};

#define SYSTEM_REGISTER_DECL(TYPE)				\
template<>										\
class SystemRegistration<TYPE>					\
{												\
public:											\
    static const RegistrySystem<TYPE>& reg;		\
};																				

#define SYSTEM_REGISTER_IMPL(TYPE)				\
const RegistrySystem<TYPE>& SystemRegistration<TYPE>::reg = RegistrySystem<TYPE>::Instance(#TYPE);	


#endif