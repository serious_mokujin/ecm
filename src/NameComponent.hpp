#ifndef NAME_COMPONENT_H
#define NAME_COMPONENT_H

#include "ComponentRegister.hpp"
#include "Component.hpp"
class Archive;

class NameComponent : public Component
{
public:
	std::string name;
	NameComponent();
	void SetName(const std::string& n);
	virtual void Init() override;
	virtual void Serialize(Archive& stream) override;
};

COMPONENT_REGISTER_DECL(NameComponent)

#endif