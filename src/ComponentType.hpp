#ifndef H_COMPONENT_TYPE
#define H_COMPONENT_TYPE

#include <unordered_map>
#include <string>
#include <vector>
#include "ETypedefs.hpp"
#include "Entity.hpp"

class Component;
class ComponentType;

// function type for create components by their names
typedef void (*AddComponentFunc)(Entity&, int);
typedef Component* (*GetComponentFunc)(Entity&, int);
typedef Component* (*GetComponentAtFunc)(int&);
typedef void (*AddToRecycleFunc)(int);
typedef void (*ClearRecycleFunc)();
typedef int (*GetBucketSizeFunc)();

struct BucketOperations
{
	AddComponentFunc	addOp;
	GetComponentFunc	getOp;
	GetComponentAtFunc	getAtOp;
	AddToRecycleFunc	recycleOp;
	ClearRecycleFunc	clearRecycleOp;
	GetBucketSizeFunc	getBucketSizeOp;
};

typedef std::unordered_map<std::string, int> NameToIdMap;
typedef std::unordered_map<int, ComponentType&> IdToTypeMap;
typedef std::unordered_map<int, BucketOperations*> IdToBucketOperationsMap;

inline NameToIdMap& getNameToIdMap()
{
	static NameToIdMap reg;
	return reg;
}
inline IdToTypeMap& getComponentTypeMap()
{
    static IdToTypeMap reg;
    return reg;
}
inline IdToBucketOperationsMap& getBucketOperationsMap()
{
	static IdToBucketOperationsMap reg;
	return reg;
}
inline BucketOperations* GetBucketOperations(const std::string& typeName)
{
	NameToIdMap& nameToIdMap = getNameToIdMap();
	IdToBucketOperationsMap& idToBucketMap = getBucketOperationsMap();
	auto it = nameToIdMap.find(typeName);
	if(it != nameToIdMap.end())
	{
		auto it2 = idToBucketMap.find(it->second);
		if(it2 != idToBucketMap.end())
			return it2->second;
	}
	return nullptr;
}
inline BucketOperations* GetBucketOperations(int typeId)
{
	IdToBucketOperationsMap& idToBucketMap = getBucketOperationsMap();
	auto it = idToBucketMap.find(typeId);
	if(it != idToBucketMap.end())
		return it->second;
	return nullptr;
}
template<class T>
void addComponent(Entity& entity, int typeId) 
{
	if(ComponentRegistration<T>::reg.m_componentRecycle->size() > 0)
	{
		int location = ComponentRegistration<T>::reg.m_componentRecycle->back();
		ComponentRegistration<T>::reg.m_componentRecycle->pop_back();

		Component* ccom = static_cast<Component*>(&ComponentRegistration<T>::reg.m_bucket->at(location));
		ccom->m_ownerID = entity.id;
		ccom->m_typeID = typeId;
		entity.SetComponentLocation(typeId, location);
		ccom->Init();
	}
	else
	{
		T com;
		Component* ccom = static_cast<Component*>(&com);
		ccom->m_ownerID = entity.id;
		ccom->m_typeID = typeId;
		ccom->Init();
		entity.SetComponentLocation(typeId, ComponentRegistration<T>::reg.m_bucket->size());
		ComponentRegistration<T>::reg.m_bucket->push_back(com);
	}
}
template<class T>
Component* getComponent(Entity& entity, int typeId) 
{
	int location = entity.GetComponentLocation(typeId);
    return &ComponentRegistration<T>::reg.m_bucket->at(location);
}
template<class T>
Component* getComponentAt(int& location) 
{
	if(location >= static_cast<int>(ComponentRegistration<T>::reg.m_bucket->size()))
	{
		location = ComponentRegistration<T>::reg.m_bucket->size();
		ComponentRegistration<T>::reg.m_bucket->push_back(T());
	}
    return &ComponentRegistration<T>::reg.m_bucket->at(location);
}
template<class T>
void recycleComponent(int componentLocation)
{
	ComponentRegistration<T>::reg.m_componentRecycle->push_back(componentLocation);
}
template<class T>
void clearRecycle()
{
	ComponentRegistration<T>::reg.m_componentRecycle->clear();
}
template<class T>
int getBucketSize()
{
	return ComponentRegistration<T>::reg.m_bucket->size();
}

class ComponentType
{
public:
	ComponentType(const std::string& name);
	static int GetComponentTypesCount();
	
	const std::string& GetName();
	int GetID();
	ComponentMask& GetBit();
private:
	static int s_componentTypesCount;
	ComponentMask m_bit;
	int m_id;
	const std::string m_name;
};
inline const std::string& ComponentType::GetName() { return m_name; }
inline int ComponentType::GetID() { return m_id; }
inline ComponentMask& ComponentType::GetBit() { return m_bit; }

template<class T>
class RegistryType
{
public:
	static RegistryType<T>& Instance()
	{
		static RegistryType<T> inst;
        return inst;
	}
	static ComponentType& TypeInstance(const std::string& name)
	{
		static ComponentType type(name);
		NameToIdMap& nameToIdMap = getNameToIdMap();
		IdToTypeMap& idToTypeMap = getComponentTypeMap();

		nameToIdMap.insert(NameToIdMap::value_type(name, type.GetID()));
		idToTypeMap.insert(IdToTypeMap::value_type(type.GetID(), type));

		IdToBucketOperationsMap& opReg = getBucketOperationsMap();
		
		static BucketOperations m_bucketOperations;

		m_bucketOperations.addOp = addComponent<T>;
		m_bucketOperations.getOp = getComponent<T>;
		m_bucketOperations.getAtOp = getComponentAt<T>;
		m_bucketOperations.recycleOp = recycleComponent<T>;
		m_bucketOperations.clearRecycleOp = clearRecycle<T>;
		m_bucketOperations.getBucketSizeOp = getBucketSize<T>;

		opReg.insert(IdToBucketOperationsMap::value_type(type.GetID(), &m_bucketOperations));

		return type;
	}
	std::vector<T>* m_bucket;
	std::vector<int>* m_componentRecycle;
private:

	RegistryType()
	{
		m_bucket = new std::vector<T>();
		m_componentRecycle = new std::vector<int>();
	}
	~RegistryType()
	{
		delete m_bucket;
		delete m_componentRecycle;
	}
};
#endif