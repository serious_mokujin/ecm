#ifndef COMPONENT_H
#define COMPONENT_H
#include <string>
class ComponentType;
class Entity;
class EWorld;
template<class T>
class ComponentRegistration;
class Archive;

class Component
{
	template<class B> friend void addComponent(Entity& entity, int typeId);
	friend class EWorld;
public:
	Component();
	virtual ~Component();
	virtual void Init() = 0;
	int GetOwnerID();
	int GetTypeID();
	virtual void Serialize(Archive& stream) = 0;

	static ComponentType* GetType(const std::string& name);
	static ComponentType* GetType(int typeId);
	template<class T>
	static const ComponentType* GetType();
protected:
private:
	int m_ownerID;
	int m_typeID;
};
template<class T>
const ComponentType* Component::GetType()
{
	return &ComponentRegistration<T>::type;
}
inline int Component::GetOwnerID() { return m_ownerID; }
inline int Component::GetTypeID() { return m_typeID; }

#endif