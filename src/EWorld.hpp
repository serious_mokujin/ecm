#ifndef EWORLD_GUARD_H
#define EWORLD_GUARD_H

#include <vector>
#include <unordered_set>
#include "Manager.hpp"
#include "Entity.hpp"

class Component;
class ComponentType;
class ESystem;
class Archive;

typedef std::vector<Component> ComponentBucket;

class EWorld : public Manager<EWorld>
{
friend class Manager<EWorld>;

public:
	void Initialize();
	void Utilize();
	int AddEntity();
	void RemoveEntity(int entityID);
	void AddTagEntity(int entityID, const std::string& tag);
	void RemoveTagEntity(int entityID, const std::string& tag);
	bool IsEntityTagged(int entityID, const std::string& tag);

	template<class T>
	void AddComponent(int entityID);
	void AddComponent(int entityID, const std::string& typeName);
	void AddComponent(int entityID, ComponentType* type);

	template<class T>
	void RemoveComponent(int entityID);
	void RemoveComponent(int entityID, const std::string& typeName);
	void RemoveComponent(int entityID, ComponentType* type);
	
	template<class T>
	T* GetComponent(int entityID);
	Component* GetComponent(int entityID, const std::string& typeName);
	Component* GetComponent(int entityID, ComponentType* type);

	void Store(Archive* stream);
	void Load(Archive* stream);

	void EnableSystem(const std::string& name);
	template<class T>
	void EnableSystem();
	void DisableSystem(const std::string& name);
	template<class T>
	void DisableSystem();
	bool IsSystemEnabled(const std::string& name);
	template<class T>
	bool IsSystemEnabled();
	ESystem* GetSystem(const std::string& name);
	template<class T>
	ESystem* GetSystem();
	
	void ProcessSystem(const std::string& name);
	template<class T>
	void ProcessSystem();

	void Update();
private:
	EWorld(void);
	~EWorld(void);
	
	std::vector<Entity> m_entityReg;
	std::vector<int> m_entityRecycle;
	std::unordered_set<int> m_entitiesNeedsUpdate;
	
};
template<class T>
void EWorld::AddComponent(int entityID)	
{ 
	AddComponent(entityID, const_cast<ComponentType*>(Component::GetType<T>())); 
}
template<class T>
void EWorld::RemoveComponent(int entityID) 
{
	RemoveComponent(entityID, const_cast<ComponentType*>(Component::GetType<T>())); 
}
template<class T>
T* EWorld::GetComponent(int entityID)
{	
	Entity& entity = m_entityReg.at(entityID);
	ComponentType* t = const_cast<ComponentType*>(Component::GetType<T>());
	if((entity.currentComponentBits & t->GetBit()).none())
		return nullptr;
	int location = entity.GetComponentLocation(t->GetID());
	return &ComponentRegistration<T>::reg.m_bucket->at(location);
}
template<class T>
void EWorld::EnableSystem()
{
	ESystem* s = getSystem<T>();
	s->Enable();
	for(auto i = m_entityReg.begin(); i != m_entityReg.end(); ++i)
		s->CheckEntity(*i);
}
template<class T>
void EWorld::DisableSystem()
{
	ESystem* s = getSystem<T>();
	s->Disable();
}
template<class T>
bool EWorld::IsSystemEnabled()
{
	ESystem* s = getSystem<T>();
	return s->IsEnabled();
}
template<class T>
ESystem* EWorld::GetSystem()
{
	return getSystem<T>();
}
template<class T>
void EWorld::ProcessSystem()
{
	getSystem<T>()->RunProcessing();
}

#endif
