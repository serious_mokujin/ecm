#pragma once
#include <vector>
#include "ETypedefs.hpp"
#include "Aspect.hpp"

class Entity;

enum class RunProcessingFlags
{
	run_per_entity = 1,
	run_whole_at_once = 2,
	run_with_each_other = 4,
	do_not_run = 8
};
using T = std::underlying_type <RunProcessingFlags>::type;
inline RunProcessingFlags operator|(RunProcessingFlags a, RunProcessingFlags b)
{
	return static_cast<RunProcessingFlags>(static_cast<T>(a) | static_cast<T>(b));
}
inline bool operator&(RunProcessingFlags a, RunProcessingFlags b)
{
	return (static_cast<T>(a) & static_cast<T>(b)) != 0;
}

typedef std::vector<int>::iterator HeapIterator;

class ESystem
{
friend class EWorld;
public:
	ESystem();
	virtual ~ESystem(void);
	void RunProcessing();
	void CheckEntity(Entity& entity);
	SystemMask& GetBit();
	bool IsEnabled() const;
	void Enable();
	void Disable();
	void SetAspect(const Aspect &aspect);
	const Aspect& GetAspect() const;
	void Clear();
protected:
	virtual void Initialize() = 0;
	virtual void Utilize() = 0;
	virtual RunProcessingFlags BeginProcessing() = 0;
	virtual void ProcessEntity(int entityID){};
	virtual void ProcessHeap(HeapIterator begin, HeapIterator end){};
	virtual void ProcessEachOther(int leftEntityID, int rightEntityID){};
	virtual void EndProcessing(){};

private:
	Aspect m_aspect;
	std::vector<int> m_entityIDs;
	SystemMask m_systemBit;
	bool m_enabled;	
};

inline bool ESystem::IsEnabled() const { return m_enabled; }
inline void ESystem::Enable() { m_enabled = true; }
inline void ESystem::Disable(){ m_enabled = false; }