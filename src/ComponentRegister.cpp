#include "ETypedefs.hpp"
#include "Entity.hpp"
#include "Archive.hpp"
#include "ComponentType.hpp"
#include "Component.hpp"
#include "ComponentRegister.hpp"

int ComponentType::s_componentTypesCount = 0;
int ComponentType::GetComponentTypesCount() { return s_componentTypesCount; }
