#ifndef VELOCITY_COMPONENT_H
#define VELOCITY_COMPONENT_H

#include "ComponentRegister.hpp"
#include "Component.hpp"
class Archive;

class VelocityComponent : public Component
{
public:
	float speedX, speedY;
	virtual void Init() override;
	virtual void Serialize(Archive& stream) override;
};

COMPONENT_REGISTER_DECL(VelocityComponent)

#endif