#ifndef H_MANAGER
#define H_MANAGER
template<class T>
class Manager
{
public:
	static T* Please()
	{
		if(s_instance == nullptr)
			s_instance = new T();
		return s_instance;
	}
	static void Thanks()
	{
		if(s_instance != nullptr)
		{
			delete s_instance;
			s_instance = nullptr;
		}
	}
private:
	static T* s_instance;
};
template<class T>
T* Manager<T>::s_instance = nullptr;
#endif