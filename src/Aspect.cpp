#include "ETypedefs.hpp"
#include "Entity.hpp"
#include "Archive.hpp"
#include "ComponentType.hpp"
#include "Component.hpp"
#include "TagManager.hpp"
#include "Aspect.hpp"

Aspect::Aspect()
{
}

Aspect& Aspect::AllOfTypes(const std::string& typeName)
{
	ComponentType* t = Component::GetType(typeName);
	allTypeMask |= t->GetBit();
	return *this;
}

Aspect& Aspect::AnyOfTypes(const std::string& typeName)
{
	ComponentType* t = Component::GetType(typeName);
	anyTypeMask |= t->GetBit();
	return *this;
}

Aspect& Aspect::NoneOfTypes(const std::string& typeName)
{
	ComponentType* t = Component::GetType(typeName);
	noneTypeMask |= t->GetBit();
	return *this;
}

Aspect& Aspect::AllOfTags(const std::string& tagName)
{
	ComponentType* t = Component::GetType(tagName);
	allTagMask |= TagManager::GetTagMask(tagName);
	return *this;
}

Aspect& Aspect::AnyOfTags(const std::string& tagName)
{
	ComponentType* t = Component::GetType(tagName);
	anyTagMask |= TagManager::GetTagMask(tagName);
	return *this;
}

Aspect& Aspect::NoneOfTags(const std::string& tagName)
{
	ComponentType* t = Component::GetType(tagName);
	noneTagMask |= TagManager::GetTagMask(tagName);
	return *this;
}

bool Aspect::Intersects(Entity& entity)
{
	if (!(allTypeMask.any() || noneTypeMask.any() || anyTypeMask.any() || 
		  allTagMask.any()  || noneTagMask.any()  || anyTagMask.any()  ))
		return false;
            
	return ((anyTypeMask & entity.currentComponentBits).any() || anyTypeMask.none()) &&
		((allTypeMask & entity.currentComponentBits) == allTypeMask || allTypeMask.none()) &&
        ((noneTypeMask & entity.currentComponentBits).none()) &&
		
		((anyTagMask & entity.tagBits).any() || anyTagMask.none()) &&
		((allTagMask & entity.tagBits) == allTagMask || allTagMask.none()) &&
        ((noneTagMask & entity.tagBits).none());
}
