#include "ETypedefs.hpp"
#include "Entity.hpp"

#include "Archive.hpp"
#include "ComponentType.hpp"
#include "Component.hpp"


#include <algorithm>
#include "TagManager.hpp"

#include "Aspect.hpp"
#include "ESystem.hpp"
#include "SystemRegister.hpp"
#include "Manager.hpp"
#include "EWorld.hpp"

EWorld::EWorld(void)
{
}
void EWorld::Initialize()
{
	SystemBucket systemReg = getSystemBucket();
	for (ESystem *system : systemReg)
	{
		system->Initialize();
	}
}
void EWorld::Utilize()
{
	SystemBucket systemReg = getSystemBucket();
	for (ESystem *system : systemReg)
	{
		system->Utilize();
	}
}
EWorld::~EWorld(void)
{
}
int EWorld::AddEntity()
{
	int id;
	if(m_entityRecycle.size() > 0)
	{
		id = m_entityRecycle.back();
		m_entityRecycle.pop_back();
		Entity& entity = m_entityReg[id];
		entity.recycled = false;
		entity.componentLocations.clear();
		entity.tagBits.reset();
	}
	else
	{
		id = m_entityReg.size();
		Entity e(id);
		e.recycled = false;
		m_entityReg.push_back(e);
	}
	return id;
}
void EWorld::RemoveEntity(int entityID)
{
	Entity& entity = m_entityReg[entityID];
	if(!entity.recycled)
	{
		m_entityRecycle.push_back(entityID);
		m_entitiesNeedsUpdate.insert(entityID);
		entity.nextComponentBits.reset();
		entity.recycled = true;
	}
}
void EWorld::AddTagEntity(int entityID, const std::string& tag)
{
	Entity& entity = m_entityReg[entityID];
	TagMask tm = TagManager::GetTagMask(tag);
	if((entity.tagBits & tm).none())
	{
		entity.tagBits |= tm;
		m_entitiesNeedsUpdate.insert(entityID);
	}
}
void EWorld::RemoveTagEntity(int entityID, const std::string& tag)
{
	Entity& entity = m_entityReg[entityID];
	TagMask tm = TagManager::GetTagMask(tag);
	if((entity.tagBits & tm).any())
	{
		entity.tagBits ^= tm;
		m_entitiesNeedsUpdate.insert(entityID);
	}
}
bool EWorld::IsEntityTagged(int entityID, const std::string& tag)
{
	Entity& entity = m_entityReg[entityID];
	return (entity.tagBits & TagManager::GetTagMask(tag)).any();
}

void EWorld::AddComponent(int entityID, const std::string& typeName)
{
	AddComponent(entityID, Component::GetType(typeName));
}
void EWorld::RemoveComponent(int entityID, const std::string& typeName)
{
	RemoveComponent(entityID, Component::GetType(typeName));
}
void EWorld::RemoveComponent(int entityID, ComponentType* type)
{
	Entity& entity = m_entityReg.at(entityID);
	ComponentMask temp(type->GetBit());
	entity.nextComponentBits &= temp.flip();
	if((entity.currentComponentBits & type->GetBit()).any())
		m_entitiesNeedsUpdate.insert(entityID);
}
void EWorld::AddComponent(int entityID, ComponentType* type)
{
	Entity& entity = m_entityReg.at(entityID);
	if((entity.currentComponentBits & type->GetBit()).none())
	{
		entity.nextComponentBits |= type->GetBit();
		m_entitiesNeedsUpdate.insert(entityID);
	}
}

Component* EWorld::GetComponent(int entityID, const std::string& typeName)
{
	return GetComponent(entityID, Component::GetType(typeName));
}
Component* EWorld::GetComponent(int entityID, ComponentType* type)
{
	Entity& entity = m_entityReg.at(entityID);
	if((entity.currentComponentBits & type->GetBit()).none())
		return nullptr;
	int location = entity.GetComponentLocation(type->GetID());
	BucketOperations* op = GetBucketOperations(type->GetID());
	if(op == nullptr)
		return nullptr;
	return op->getOp(entity, type->GetID());
}

void EWorld::EnableSystem(const std::string& name)
{
	ESystem* s = getSystem(name);
	s->Enable();
	for(auto i = m_entityReg.begin(); i != m_entityReg.end(); ++i)
		s->CheckEntity(*i);
}
void EWorld::DisableSystem(const std::string& name)
{
	ESystem* s = getSystem(name);
	s->Disable();
	s->Clear();
	SystemMask temp(s->m_systemBit);
	temp.flip();
	for(auto i = m_entityReg.begin(); i != m_entityReg.end(); ++i)
		i->systemBits &= temp;
}
bool EWorld::IsSystemEnabled(const std::string& name)
{
	ESystem* s = getSystem(name);
	return s->IsEnabled();
}
ESystem* EWorld::GetSystem(const std::string& name)
{
	return getSystem(name);
}
void EWorld::ProcessSystem(const std::string& name)
{
	getSystem(name)->RunProcessing();
}

void EWorld::Update()
{
	if(m_entitiesNeedsUpdate.size() > 0)
	{
		for(auto i = m_entitiesNeedsUpdate.begin(); i != m_entitiesNeedsUpdate.end(); ++i)
		{
			Entity& entity = m_entityReg[*i];
			ComponentMask oldMask(entity.currentComponentBits);
			ComponentMask newMask(entity.nextComponentBits);

			ComponentMask toAdd = oldMask.flip() & newMask;
			ComponentMask toRemove = oldMask.flip() & newMask.flip();
			
			ComponentMask bit;
			bit.set(0);
			int componentCount = ComponentType::GetComponentTypesCount();
			for(int j = 0; j < componentCount; ++j) // j is a typeId, types were defined sequentially
			{
				if((toAdd & bit).any())
				{
					BucketOperations* op = GetBucketOperations(j);
					op->addOp(entity, j);
				}
				else if((toRemove & bit).any())
				{
					BucketOperations* op = GetBucketOperations(j);
					op->recycleOp(entity.GetComponentLocation(j));
				}
				bit <<= 1;
			}
			entity.currentComponentBits = entity.nextComponentBits;
			SystemBucket& reg = getSystemBucket();
			for(auto k = reg.begin(); k != reg.end(); ++k)
				if((*k)->IsEnabled())
					(*k)->CheckEntity(entity);
		}
		m_entitiesNeedsUpdate.clear();
	}
}

void EWorld::Store(Archive* _stream)
{
	Archive& stream = *_stream;

	if(m_entitiesNeedsUpdate.size() > 0)
		Update();
	//------- Write world tags first
	int tagCount = TagManager::m_tags.size();
	stream<<tagCount;
	for(auto i = TagManager::m_tags.begin(); i != TagManager::m_tags.end(); ++i)
	{
		stream<<i->first<<i->second;
	}
	//-------------------------
	int entity_count = static_cast<int>(m_entityReg.size());
	stream<<entity_count;

	for(int i = 0; i < entity_count; ++i)
	{
		Entity& entity = m_entityReg[i];
		int componentCount = entity.currentComponentBits.count();
		stream<<entity.id<<entity.tagBits<<componentCount;
		
		ComponentMask m;
		m.set(0);
		for(int j = 0; j < ComponentType::GetComponentTypesCount(); j++)
		{
			if((entity.currentComponentBits & m).any())
			{
				int location = entity.componentLocations[j];
				BucketOperations* op = GetBucketOperations(j);
				stream<<j;
				op->getOp(entity, j)->Serialize(stream);
			}
			m <<= 1;
		}
	}
}
void EWorld::Load(Archive* _stream)
{
	Archive& stream = *_stream;
	//------- Read world tags first
	TagManager::m_tags.clear();
	int tagCount = 0;
	stream>>tagCount;
	std::string tempStr;
	TagMask tempMask;
	for(int i = 0; i < tagCount; ++i)
	{
		
		stream>>tempStr;
		stream>>tempMask;
		TagManager::m_tags.insert(std::unordered_map<std::string, TagMask>::value_type(tempStr, tempMask));
	}
	//-------------------------
	int entitiesCount = 0;
	stream>>entitiesCount;
	int componentOffset[MAX_COMPONENTS];
	memset(&componentOffset, 0, sizeof(int)*MAX_COMPONENTS);
	SystemBucket& systemReg = getSystemBucket();
	for(auto k = systemReg.begin(); k != systemReg.end(); ++k)
		if((*k)->IsEnabled())
			(*k)->Clear();
	for(int i = 0; i < entitiesCount; ++i)
	{
		int id = 0;		stream>>id; // useless read
		TagMask tm;		stream>>tm;
		int componentCount = 0;	stream>>componentCount;
		if(i >= (int)m_entityReg.size())
		{
			Entity e(i);
			m_entityReg.push_back(e);
		}
		Entity& entity = m_entityReg[i];
		entity.recycled = false;
		entity.tagBits = tm;
		entity.componentLocations.clear();
		entity.currentComponentBits.reset();
		entity.systemBits.reset();
		for(int j = 0; j < componentCount; j++)
		{
			int componentTypeId = 0;
			stream>>componentTypeId;
			BucketOperations* op = GetBucketOperations(componentTypeId);
			int location = componentOffset[componentTypeId]++;
			Component* com = op->getAtOp(location);
			com->m_ownerID = i;
			com->m_typeID = componentTypeId;
			com->Init();
			com->Serialize(stream);
			entity.SetComponentLocation(componentTypeId, location);
			ComponentType* type = Component::GetType(componentTypeId);
			entity.currentComponentBits |= type->GetBit();
		}
		entity.nextComponentBits = entity.currentComponentBits;
		for(auto k = systemReg.begin(); k != systemReg.end(); ++k)
			if((*k)->IsEnabled())
				(*k)->CheckEntity(entity);
	}
	// ---- now recycle all components left in world unused
	for(int j = 0; j < ComponentType::GetComponentTypesCount(); j++)
	{
		BucketOperations* op = GetBucketOperations(j);
		op->clearRecycleOp();
		int bsize = op->getBucketSizeOp();
		for(int i = componentOffset[j]; i < bsize; ++i)
		{
			op->recycleOp(i);
		}
	}
	// ---- now recycle all entities left in world unused
	m_entityRecycle.clear();
	for(int i = entitiesCount; i < static_cast<int>(m_entityReg.size()); ++i)
	{
		Entity& entity = m_entityReg[i];
		m_entityRecycle.push_back(i);
		entity.componentLocations.clear();		// we don't need to recycle components attached to this entity,
		entity.currentComponentBits.reset();	// because we already recycled them all.
		entity.nextComponentBits.reset();
		entity.systemBits.reset();
		entity.recycled = true;
	}
	
	m_entitiesNeedsUpdate.clear();
}

	