#include "NameComponent.hpp"
#include "Archive.hpp"

COMPONENT_REGISTER_IMPL(NameComponent)

NameComponent::NameComponent() : name("")
{	
}

void NameComponent::SetName(const std::string& n)
{
	this->name = n;
}

void NameComponent::Init()
{
	name = "";
}

void NameComponent::Serialize(Archive& stream)
{
	if(stream.GetDirection() == Archive::Direction::Store)
		stream<<name;
	else
		stream>>name;
}