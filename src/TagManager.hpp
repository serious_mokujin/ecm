#ifndef TAG_MANAGER_H
#define TAG_MANAGER_H

#include <unordered_map>
#include <string>
//#include "ComponentModel.hpp"

class TagManager
{
friend class EWorld;
public:
	static TagMask GetTagMask(const std::string& tagName);

private:
	static std::unordered_map<std::string, TagMask> m_tags;
};

#endif