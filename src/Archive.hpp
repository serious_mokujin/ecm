#ifndef ARCHIVE_HEADER
#define ARCHIVE_HEADER

#include "ETypedefs.hpp"

class Archive
{
public:
	enum class Direction
	{
		Store,
		Load
	};

public:
	Archive(Direction direction);
	virtual ~Archive();

    virtual void write(const void* buffer, size_t length) {}
    virtual void read(void* buffer, size_t length) {}
	virtual void end(){}
    Archive& operator<<(const std::string& str);
    Archive& operator>>(std::string& str);

    Archive& operator<<(const int val);
    Archive& operator>>(int& val);

	Archive& operator<<(float val);
    Archive& operator>>(float& val);

	Archive& operator<<(const ComponentMask& val);
    Archive& operator>>(ComponentMask& val);

	Archive& operator>>(TagMask& val);
	Archive& operator<<(const TagMask& val);

	Direction GetDirection() const;

private:
	Direction m_direction;
};

inline Archive::Direction Archive::GetDirection() const
{ 
	return m_direction; 
}

#endif