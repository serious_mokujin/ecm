#include "VelocityComponent.hpp"
#include "Archive.hpp"

COMPONENT_REGISTER_IMPL(VelocityComponent)

void VelocityComponent::Init()
{
	speedX = 4.2f;
	speedY = 1.6f;
}

void VelocityComponent::Serialize(Archive& stream)
{
	if(stream.GetDirection() == Archive::Direction::Store)
		stream<<speedX<<speedY;
	else
		stream>>speedX>>speedY;
}